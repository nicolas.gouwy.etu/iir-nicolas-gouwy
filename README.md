# Projet IIR - _Rendre conflict-free l'édition asynchrone de texte collaboratif._

## 1. Binôme

_Gouwy Nicolas nicolas.gwy@gmail.com ou nicolas.gouwy.etu@univ-lille.fr_

## 2. Article de référence

-   **Citation :** SHAPIRO, Marc, PREGUIÇA, Nuno, BAQUERO, Carlos, et al. Conflict-free replicated data types. In : Stabilization, Safety, and Security of Distributed Systems: 13th International Symposium, SSS 2011, Grenoble, France, October 10-12, 2011. Proceedings 13. Springer Berlin Heidelberg, 2011. p. 386-400.

-   **Conférence :** Symposium on Stabilization, Safety, and Security of Distributed Systems (SSS)

-   **Classification :** C (Conference Ranks)

-   **Nombre de citations :** 1052 (Google Scholar)

## 3. Résumé de l'article de référence

**Problématique**

La réplication des données, basées sur l'état ou les opérations, dans un système réparti avec des capacités de communication limitées.

**Pistes possibles**

Les CRDT (Type de données répliquées sans conflit), des structures de données répliquées qui garantissent la convergence sans conflit des données répliquées, indépendamment de l'ordre d'exécution des opérations. L'article parle de deux types de CRDT : les CRDT opérationnels et les CRDT étatiques.

**Question de recherche**

-   Comment éviter la complexité de la résolution des conflits et du _roll-back_.
-   Comment conserver ou améliorer la disponibilité et réduire la latence du système, tout en évitant une synchronisation coûteuse et une approche basée sur le consensus
-   Élargir nos connaissances sur les principes et la pratique des CRDT.

**Démarche adoptée**

-   Preuves formelles
-   Expérience de pensée et implémentation

**Implémentation de la démarche**

Plusieurs théorèmes sont énoncés et prouvés mathématiquement. Une expérience de pensée sur l'utilisation d'un CRDT d'ensemble (_set_) pour un graphe orienté pour un moteur de recherche est également présenté avec un exemple d'implémentation.

**Les résultats**

-   Une solution au problème CAP, la Strong Eventual Consistency (SEC).
-   Définitions formelles de la Strong Eventual Cohérence (SEC) et des CRDT.
-   Deux conditions suffisantes pour SEC.
-   Une forte équivalence entre les deux conditions.
-   Nous montrons que la SEC est incomparable à la cohérence séquentielle.
-   Description des CRDT de base, y compris les vecteurs entiers et les compteurs.
-   CRDT plus avancés, avec notemment des ensembles (_sets_) et des graphs.

## 4. Articles connexes

### Article 1

<details>
<summary>Cliquer pour voir</summary>

#### Référence & indicateur

-   **Citation :** PREGUIÇA, Nuno, MARQUÈS, Joan Manuel, SHAPIRO, Marc, et al. A commutative replicated data type for cooperative editing. In : 2009 29th IEEE International Conference on Distributed Computing Systems. IEEE, 2009. p. 395-403.

-   **Conférence / Revue :** IEEE International Conference on Distributed Computing Systems

-   **Classification :** A1 (Conference Ranks)

-   **Nombre de citations :** 328 (Google Scholar)

> **Note :** Cet article est cité par l'article de référence, car il s'agit des prémisses de la méthode CRDT qui signifiait alors "Convergent Replicated Data Type" et qui est maintenant appelée "Conflict-free Replicated Data Type".

#### Résumé

**Problématique**

La méthode CRDT pour l'édition coopérative de texte.

**Pistes possibles**

-   La méthode CRDT pour concevoir des types de données répliqués, assurant la convergence sans contrôle de concurrence complexe. Nous illustrons la méthode CRDT avec un exemple utile et concret : un partage tampon d'édition séquentielle. Nous identifions les exigences pour le tampon d'édition, en particulier, un espace d'identification dense.

-   Une mise en œuvre pratique et efficace d'un espace d'identification dense, basé sur un arbre binaire étendu.

-   Un certain nombre d'optimisations qui complètent la surcharge d'identifiant et de stockage.

-   Un mécanisme de compactage distribué qui minimise la taille de l'identifiant et supprime la surcharge de stockage.

**Question de recherche**

Un type de données répliquées pour l'édition coopérative qui est commutatif, convergent et qui permet une édition concurrente sans conflit.

**Démarche adoptée**

Validation de la conception par une étude comparative, basée sur les traces des historiques d'édition existants.

**Implémentation de la démarche**

Notre objectif est de mesurer les frais généraux de Treedoc et d'évaluer les effets de certaines alternatives de conception : granularité des atomes, conception de désambiguïsation (UDIS vs. SDIS) et heuristiques d'aplatissement.

Pour garantir une évaluation réaliste, nous rejouons les sessions d'édition coopérative extraites des référentiels existants. Nous avons analysé un grand nombre de charges de travail différentes : modifications de fichiers sources C++ à partir du référentiel SVN du projet open source KDE ; de fichiers sources Java et Latex à partir d'un référentiel SVN privé ; et des pages Wikipédia.

**Les résultats**

-   Treedoc a une surcharge inférieure à celle de l'alternative Logoot, et raisonnable en termes absolus.

-   Bien Les SDIS soent moins volumineux par noeud, la supression de "tombes" est moins fréquente, ce qui entraîne une surcharge de stockage plus importante que celle des UDIS.

-   L'heuristique choisie pour l'aplatissement a l'air d'être à blâmer. Cependant, lorsque l'applatissage est désactivé, 95% des noeuds sont des tombes, ce qui entraîne une surcharge de stockage importante.

</details>

### Article 2

<details>
<summary>Cliquer pour voir</summary>

#### Référence & indicateur

-   **Citation :** LETIA, Mihai, PREGUIÇA, Nuno, et SHAPIRO, Marc. Consistency without concurrency control in large, dynamic systems. ACM SIGOPS Operating Systems Review, 2010, vol. 44, no 2, p. 29-34.

-   **Conférence / Revue :** ACM SIGOPS Operating Systems Review

-   **Classification :** Introuvée.

-   **Nombre de citations :** 53 (Google Scholar)

> **Note :** Cet article est cité par l'article de référence, car il s'agit des prémisses de la méthode CRDT qui signifiait alors "Convergent Replicated Data Type" et qui est maintenant appelée "Conflict-free Replicated Data Type".

#### Résumé

**Problématique**

Cohérence sans contrôle de concurrence dans les grands systèmes dynamiques.

**Pistes possibles**

-   Validation de la conception avec des mesures de performances d'un benchmark Wikipédia exigeant.
-   Pour l'évolutivité, une proposition d'architecture flexible à deux niveaux : un petit noyau stable prend en charge à la fois les mises à jour et le consensus. Elle coexiste avec une nébuleuse dynamique illimitée, incontrôlée, ne supportant que les mises à jour.
-   Présentation d'un nouveau protocole qui permet à un site nébuleuse de rattraper les consensus passés du noyau, afin d'envoyer ses mises à jour au noyau, et éventuellement de migrer dans le noyau.
-   La section 6 discute des enseignements tirés et des généralisations possibles.

**Question de recherche**

Concevoir des types de données partagés pour la commutativité.

**Démarche adoptée**

**Implémentation de la démarche**

**Les résultats**

</details>

### Article 3

<details>
<summary>Cliquer pour voir</summary>

#### Référence & indicateur

-   **Citation :** BRIOT, Loïck, URSO, Pascal, et SHAPIRO, Marc. High responsiveness for group editing CRDTs. In : Proceedings of the 2016 ACM International Conference on Supporting Group Work. 2016. p. 51-60.

-   **Conférence / Revue :** ACM International Conference on Supporting Group Work

-   **Classification :** Introuvée

-   **Nombre de citations :** 34 (Google Scholar)

> **Note :** M. Shapiro est également l'auteur de l'article de référence et des atricles 1 & 2. Cet article m'intéresse car il date de 2016 et présente un état de l'art "rétrospectif" sur l'édition collaborative en section 2.

#### Résumé

**Problématique**

La réactivité de l'édition collaborative grâce à la réplication optimiste.

**Pistes possibles**

Une approche pour améliorer la réactivité des algorithmes de CRDT basée sur une structure d'identification additionnelle

**Question de recherche**

Comment améliorer la réactivité des algorithmes de CRDT pour l'édition collaborative, en particulier concernant les opérations _upstream_, afin de garantir que l'utilisateur voit son édition s'afficher immédiatement.

**Démarche adoptée**

Évaluation de l'approche en comparant les implémentations des solutions présentées dans l'article avec les implémentations des meilleurs algorithmes de réplication comparables.

**Implémentation de la démarche**

-   Utilisation d'un framework d'analyse comparative Java dans lequel les auteurs des autres algorithmes ont implémenté ces derniers.

-   Implémentation de quatre nouveaux algorithmes : RGA avec une structure d'identifiant, Logoot avec une structure d'identifiant, RGA avec des blocs et RGATreeSplit.

**Les résultats**

Les expériences démontrent que l'approche proposée atteint les meilleures performances globales parmi les algorithmes CRDT existants. Plus particulièrement, Elle améliore considérablement les performances d'intégration des modifications des utilisateurs.

</details>

### Article 4

<details>
<summary>Cliquer pour voir</summary>

#### Référence & indicateur

-   **Citation :** WEISS, Stéphane, URSO, Pascal, et MOLLI, Pascal. Logoot: A scalable optimistic replication algorithm for collaborative editing on p2p networks. In : 2009 29th IEEE International Conference on Distributed Computing Systems. IEEE, 2009. p. 404-412.

-   **Conférence / Revue :** IEEE International Conference on Distributed Computing Systems

-   **Classification :** A1 (Conference Ranks)

-   **Nombre de citations :** 191 (Google Scholar)

#### Résumé

**Problématique**

La réplication optimiste pour l'édition collaborative sur les réseaux P2P.

**Pistes possibles**

Présentation d'un nouvel algorithme de réplication optimiste appelé Logoot qui assure la cohérence CCI pour les structures linéaires, qui tolère un grand nombre de copies et qui ne nécessite pas l'utilisation de _tombstones_. Cette approche est basée sur des identifiants de position d'objet non mutables et totalement ordonnés. La complexité temporelle de Logoot n'est que logarithmique en fonction de la taille du document.

**Question de recherche**

Comment implémenter un algorithme de réplication optimiste qui garantit la cohérence CCI sur les structures linéaires

**Démarche adoptée**

Validation de l'algorithme Logoot avec des données réelles extraites de Wikipédia.

**Implémentation de la démarche**

Pour rejouer les historiques de certaines pages Wikipédia, l'auteur a utilisé l'API MediaWiki3 pour obtenir un fichier XML contenant plusieurs révisions d'une page Wikipédia spécifique. Puis calcule, à l'aide d'un algorithme diff, les modifications effectuées entre deux révisions. Les modifications sont simplement réexécutées dans le modèle proposé.

**Les résultats**

L'expérimentation démontre que la liste illimitée d'identifiants Logoot associés à chaque ligne reste acceptable en pratique. L'expérimentation montre également que Logoot a de meilleures performances moyennes que les algorithmes WOOT et Treedoc.

</details>

## 5. Grille d'analyse

|                 | Algorithmes | Types de données     | Présence de tombstones | Performances en _upstream_ | Performances en _downstream_ |
| --------------- | ----------- | -------------------- | ---------------------- | -------------------------- | ---------------------------- |
| Article de Ref  | CRDT        | Tout type            | Oui                    | Mauvaises                  | Bonnes                       |
| Article Connx 1 | CRDT        | Principalement texte | Oui                    | Mauvaises                  | Bonnes                       |
| Article Connx 2 | CRDT        | Tout type            | Oui                    | Mauvaises                  | Bonnes                       |
| Article Connx 3 | CRDT        | Tout type            | Oui                    | Bonnes                     | Bonnes                       |
| Article Connx 4 | Logoot      | Principalement Texte | Non                    | Bonnes                     | Mauvaises                    |

## 6. Question de recherche

Notre travail de recherche vise à étudier les méthodes de réplication optimiste pour l'édition collaborative de texte afin de déterminer si celle-ci peut être rendue sans conflit.

## 7. Plan de la démarche adoptée

Pour notre question de recherche, nous allons effectuer une expérience contrôlée en laboratoire.

Cela nous permettra de ...

Le protocole est le suivant ...
